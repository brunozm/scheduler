package com.projects.scheduler

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SchedulerApplication {

	static void main(String[] args) {
		SpringApplication.run(SchedulerApplication, args)
	}

}
