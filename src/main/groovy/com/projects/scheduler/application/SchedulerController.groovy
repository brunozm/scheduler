package com.projects.scheduler.application

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class SchedulerController {

    @GetMapping("/v1/scheduler")
    SuccessResult get() {
        SuccessResult result = new SuccessResult(response: "funcionou!!")
        return result
    }
}
